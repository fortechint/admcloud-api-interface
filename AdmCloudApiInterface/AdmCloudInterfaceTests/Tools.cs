﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdmCloudInterfaceTests
{
    public class Tools
    {
        //Unit tests credentials
        private static string email = "";
        private static string password = "";
        private static string companyID = "";
        private static string roleName = "";
        private static Guid roleID = Guid.Parse("");
        private static Guid appID = Guid.Parse("");

        public async static Task<string> GetToken()
        {
            //Call API to get token
            var loginRepository = new AdmCloudInterface.Repositories.LoginRepository
            (
                email: email,
                password: password,
                company: companyID,
                role: roleName,
                roleid: roleID,
                appid: appID
            );

            return await loginRepository.GetTokenAsync();
        }
    }
}
