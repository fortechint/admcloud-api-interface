﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdmCloudInterface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdmCloudInterfaceTests;

namespace AdmCloudInterface.Repositories.Tests
{
    [TestClass()]
    public class CreditInvoicesRepositoryTests
    {
        [TestMethod()]
        public async Task RunAll()
        {
            var token = await Tools.GetToken();

            var repository = new AdmCloudInterface.Repositories.CreditInvoicesRepository(token);

            var creditInvoice = new Models.CreditInvoice();

            //Get customer
            {
                var customersRepository = new AdmCloudInterface.Repositories.CustomersRepository(token);

                //Get list
                var list = await customersRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered customers");

                creditInvoice.RelationshipID = firstRecord.ID;
            }

            //Get location
            {
                var locationsRepository = new AdmCloudInterface.Repositories.LocationsRepository(token);

                //Get list
                var list = await locationsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered locations");

                creditInvoice.LocationID = firstRecord.ID;
            }

            //Get currency
            {
                var currenciesRepository = new AdmCloudInterface.Repositories.CurrenciesRepository(token);

                //Get list
                var list = await currenciesRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered currencies");

                creditInvoice.CurrencyID = firstRecord.ID;
            }

            //Get payment term
            {
                var paymentTermsRepository = new AdmCloudInterface.Repositories.PaymentTermsRepository(token);

                //Get list
                var list = await paymentTermsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered payment terms");

                creditInvoice.PaymentTermID = firstRecord.ID;
            }

            creditInvoice.Items.Add(new Models.CreditInvoice_Item
            {
                Name = "My credit invoice from the API",
                Quantity = 1,
                Price = 5500
            });

            //Insert record
            creditInvoice.ID = await repository.InsertAsync(record: creditInvoice);

            //Get record
            creditInvoice = await repository.GetAsync(ID: creditInvoice.ID);

            //Update record
            creditInvoice.Reference = "New reference from the API!";
            await repository.UpdateAsync(record: creditInvoice);

            //Get list
            await repository.GetListAsync();

            //Delete record
            await repository.DeleteAsync(ID: creditInvoice.ID);
        }
    }
}