﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdmCloudInterface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdmCloudInterfaceTests;

namespace AdmCloudInterface.Repositories.Tests
{
    [TestClass()]
    public class CustomerCreditNotesRepositoryTests
    {
        [TestMethod()]
        public async Task RunAll()
        {
            var token = await Tools.GetToken();

            var repository = new AdmCloudInterface.Repositories.CustomerCreditNotesRepository(token);

            var creditNote = new Models.CustomerCreditNote();

            //Get customer
            {
                var customersRepository = new AdmCloudInterface.Repositories.CustomersRepository(token);

                //Get list
                var list = await customersRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered customers");

                creditNote.RelationshipID = firstRecord.ID;
            }

            //Get location
            {
                var locationsRepository = new AdmCloudInterface.Repositories.LocationsRepository(token);

                //Get list
                var list = await locationsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered locations");

                creditNote.LocationID = firstRecord.ID;
            }

            //Get currency
            {
                var currenciesRepository = new AdmCloudInterface.Repositories.CurrenciesRepository(token);

                //Get list
                var list = await currenciesRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered currencies");

                creditNote.CurrencyID = firstRecord.ID;
            }

            creditNote.Items.Add(new Models.CustomerCreditNote_Item
            {
                Name = "My customer credit note from the API",
                Quantity = 1,
                Price = 5500
            });

            //Insert record
            creditNote.ID = await repository.InsertAsync(record: creditNote);

            //Get record
            creditNote = await repository.GetAsync(ID: creditNote.ID);

            //Update record
            creditNote.Reference = "New reference from the API!";
            await repository.UpdateAsync(record: creditNote);

            //Get list
            await repository.GetListAsync();

            //Delete record
            await repository.DeleteAsync(ID: creditNote.ID);
        }
    }
}