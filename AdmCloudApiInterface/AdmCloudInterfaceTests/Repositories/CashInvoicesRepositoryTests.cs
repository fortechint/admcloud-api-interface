﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdmCloudInterface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdmCloudInterfaceTests;

namespace AdmCloudInterface.Repositories.Tests
{
    [TestClass()]
    public class CashInvoicesRepositoryTests
    {
        [TestMethod()]
        public async Task RunAll()
        {
            var token = await Tools.GetToken();

            var repository = new AdmCloudInterface.Repositories.CashInvoicesRepository(token);

            var invoice = new Models.CashInvoice();

            //Get customer
            {
                var customersRepository = new AdmCloudInterface.Repositories.CustomersRepository(token);

                //Get list
                var list = await customersRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered customers");

                invoice.RelationshipID = firstRecord.ID;
            }

            //Get location
            {
                var locationsRepository = new AdmCloudInterface.Repositories.LocationsRepository(token);

                //Get list
                var list = await locationsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered locations");

                invoice.LocationID = firstRecord.ID;
            }

            //Get currency
            {
                var currenciesRepository = new AdmCloudInterface.Repositories.CurrenciesRepository(token);

                //Get list
                var list = await currenciesRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered currencies");

                invoice.CurrencyID = firstRecord.ID;
            }

            //Get cash account
            {
                var accountsRepository = new AdmCloudInterface.Repositories.AccountsRepository(token);

                //Get list
                var list = await accountsRepository.GetListAsync();

                //Get cash or bank accounts from list
                var bankAccounts = list.Where(t => t.AccountClassID == 100 || t.AccountClassID == 110).ToList();

                //Get first record from list
                var firstRecord = bankAccounts.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered cash or bank accounts");

                invoice.CashAccountID = firstRecord.ID;
            }

            //Get payment method
            {
                var paymentMethodsRepository = new AdmCloudInterface.Repositories.PaymentMethodsRepository(token);

                //Get list
                var list = await paymentMethodsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered payment methods");

                invoice.PaymentMethodID = firstRecord.ID;
            }

            invoice.Items.Add(new Models.CashInvoice_Item
            {
                Name = "My cash invoice from the API",
                Quantity = 1,
                Price = 5500
            });

            //Insert record
            invoice.ID = await repository.InsertAsync(record: invoice);

            //Get record info
            invoice = await repository.GetAsync(ID: invoice.ID);

            //Update record
            invoice.Reference = "New reference from the API!";
            await repository.UpdateAsync(record: invoice);

            //Get list
            await repository.GetListAsync();

            //Delete record
            await repository.DeleteAsync(ID: invoice.ID);
        }
    }
}