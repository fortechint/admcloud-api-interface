﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdmCloudInterface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdmCloudInterfaceTests;

namespace AdmCloudInterface.Repositories.Tests
{
    [TestClass()]
    public class SalesOrdersRepositoryTests
    {
        [TestMethod()]
        public async Task RunAll()
        {
            //Get API token
            var token = await Tools.GetToken();

            var repository = new AdmCloudInterface.Repositories.SalesOrdersRepository(token);

            //Create new sales order
            var salesOrder = new AdmCloudInterface.Models.SalesOrder();

            //Get customer
            {
                var customersRepository = new AdmCloudInterface.Repositories.CustomersRepository(token);
                
                //Get list
                var list = await customersRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered customers");

                salesOrder.RelationshipID = firstRecord.ID;
            }

            //Get location
            {
                var locationsRepository = new AdmCloudInterface.Repositories.LocationsRepository(token);

                //Get list
                var list = await locationsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered locations");

                salesOrder.LocationID = firstRecord.ID;
            }

            //Get payment term
            {
                var paymentTermsRepository = new AdmCloudInterface.Repositories.PaymentTermsRepository(token);

                //Get list
                var list = await paymentTermsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered payment terms");

                salesOrder.PaymentTermID = firstRecord.ID;
            }

            //Get currency
            {
                var currenciesRepository = new AdmCloudInterface.Repositories.CurrenciesRepository(token);

                //Get list
                var list = await currenciesRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered currencies");

                salesOrder.CurrencyID = firstRecord.ID;
            }

            //Get item
            {
                var itemsRepository = new AdmCloudInterface.Repositories.ItemsRepository(token);

                //Get list
                var list = await itemsRepository.GetListAsync();

                //Get first record from list
                var firstRecord = list.FirstOrDefault();
                if (firstRecord == null)
                    throw new ApplicationException("There are no registered items");

                //Get item information
                var item = await itemsRepository.GetAsync(ID: firstRecord.ID);

                salesOrder.Items.Add(new Models.SalesOrder_Item
                {
                    ItemID = item.ID,
                    Quantity = 10,
                    UOMID = item.SalesUOMID,
                    Price = 1000
                });
            }

            //Insert sales order
            salesOrder.ID = await repository.InsertAsync(record: salesOrder);

            //Authorize sales order
            await repository.AuthorizeAsync(ID: salesOrder.ID);

            //Get sales order info
            salesOrder = await repository.GetAsync(ID: salesOrder.ID);

            //Update sales order
            salesOrder.Reference = "New reference from the API!";
            await repository.UpdateAsync(record: salesOrder);

            //Get list
            await repository.GetListAsync();

            //Delete sales order
            await repository.DeleteAsync(ID: salesOrder.ID);
        }
    }
}