﻿using System;
using System.Windows.Forms;
using System.Linq;
using AdmCloudInterface.Models;
using System.Threading.Tasks;

namespace AdmCloudApiDemoClient
{
    public partial class Form1 : Form
    {
        //INSTRUCCIONES
        //Antes de usar este demo, debe crear una integración en su cuenta de Adm Cloud

        public Form1()
        {
            InitializeComponent();
        }

        private void _LogInformation(string message)
        {
            txtLog.Text = "";
            txtLog.Text = message;
        }

        private async Task<string> _GetToken()
        {
            var email = txtEMail.Text;
            var password = txtPassword.Text;
            var companyID = txtCompanyID.Text;
            var roleName = txtRoleName.Text;
            var sRoleID = txtRoleID.Text;
            var sAppID = txtAppID.Text;

            if (string.IsNullOrEmpty(email))
            {
                throw new ApplicationException("You must specify an email address.");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ApplicationException("You must specify a password.");
            }

            if (string.IsNullOrEmpty(companyID))
            {
                throw new ApplicationException("You must specify a company ID.");
            }

            if (string.IsNullOrEmpty(roleName))
            {
                throw new ApplicationException("You must specify a role name.");
            }

            //Try to parse the app id
            Guid.TryParse(sAppID, out Guid appID);
            if (string.IsNullOrEmpty(sAppID))
            {
                throw new ApplicationException("You must specify an app ID.");
            }
            else if (appID == Guid.Empty)
            {
                throw new ApplicationException("Specified app id is invalid.");
            }

            //Try to parse the role id
            Guid.TryParse(sRoleID, out Guid roleID);
            if (string.IsNullOrEmpty(sRoleID))
            {
                throw new ApplicationException("You must specify a role ID.");
            }
            else if (appID == Guid.Empty)
            {
                throw new ApplicationException("Specified role id is invalid.");
            }

            //Call API to get token
            var loginRepository = new AdmCloudInterface.Repositories.LoginRepository
            (
                email: email,
                password: password,
                company: companyID,
                role: roleName,
                roleid: roleID,
                appid: appID
            );

            return await loginRepository.GetTokenAsync();
        }

        private async void btnListarClientes_Click(object sender, EventArgs e)
        {
            try
            {
                var token = await _GetToken();

                //Create repository
                var repository = new AdmCloudInterface.Repositories.CustomersRepository(token: token);

                //Get customers list
                var customers = await repository.GetListAsync(0);

                string message = "";
                foreach (var customer in customers)
                {
                    message += customer.FullName + " - " + customer.EMail + Environment.NewLine;
                }

                _LogInformation(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private async void btnObtenerDetalles_Click(object sender, EventArgs e)
        {
            try
            {
                var token = await _GetToken();

                //Create repository
                var repository = new AdmCloudInterface.Repositories.CustomersRepository(token: token);

                //Get customers list
                var customers = await repository.GetListAsync();

                //Get first customer from list
                var first_customer = customers.FirstOrDefault();

                //Get first customer information
                var customer_from_get = await repository.GetAsync(first_customer.ID);

                _LogInformation(customer_from_get.FullName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message);
            }
        }

        private async void btnCreateJournal_Click(object sender, EventArgs e)
        {
            try
            {
                var token = await _GetToken();

                //Get Accounts
                var accounts_repository = new AdmCloudInterface.Repositories.AccountsRepository(token: token);

                var accounts = await accounts_repository.GetListAsync();

                var ar_account = accounts.FirstOrDefault(t => t.AccountClassID == 120); //Accounts Receivable
                var sales_account = accounts.FirstOrDefault(t => t.AccountClassID == 400); //Sales

                if (ar_account == null)
                    throw new ApplicationException("Company must have a receivables account on the general ledger");

                if (sales_account == null)
                    throw new ApplicationException("Company must have a sales account on the general ledger");

                //Create Journal
                var journal_repository = new AdmCloudInterface.Repositories.JournalsRepository(token: token);

                var journal = new Journal();
                journal.DocDate = DateTime.Today;
                journal.CurrencyID = "DOP";
                journal.ExchangeRate = 1;

                journal.Accounts.Add(new Journal_Account { AccountID = ar_account.ID, Debit = 100 });
                journal.Accounts.Add(new Journal_Account { AccountID = sales_account.ID, Credit = 100 });

                var id = await journal_repository.InsertAsync(journal);

                _LogInformation($"Internal ID of the registered transaction: {id.ToString()}");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message);
            }
        }

        private async void btnListItems_Click(object sender, EventArgs e)
        {
            try
            {
                var token = await _GetToken();

                var repository = new AdmCloudInterface.Repositories.ItemsRepository(token: token);

                var items = await repository.GetListAsync(0);

                string message = "";

                foreach (var item in items)
                {
                    message += $"{item.SKU} - {item.Name}" + Environment.NewLine;
                }

                _LogInformation(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message);
            }
        }

        private async void btnCreateInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                var token = await _GetToken();

                //Get Customer
                var customer_repository = new AdmCloudInterface.Repositories.CustomersRepository(token: token);

                var customers = await customer_repository.GetListAsync();

                var customer = customers.FirstOrDefault();

                if (customer == null)
                    throw new Exception("There are no customers");

                //Get Item
                var item_repositories = new AdmCloudInterface.Repositories.ItemsRepository(token: token);

                var items = await item_repositories.GetListAsync();

                var item = items.FirstOrDefault();

                if (item == null)
                    throw new Exception("There are no items");

                //Get Payment Terms
                var terms_repositories = new AdmCloudInterface.Repositories.PaymentTermsRepository(token: token);

                var terms = await terms_repositories.GetListAsync();

                var term = terms.FirstOrDefault();

                if (term == null)
                    throw new Exception("There are no payment terms");

                //Get Location
                var locations_repositories = new AdmCloudInterface.Repositories.LocationsRepository(token: token);

                var locations = await locations_repositories.GetListAsync();

                var location = locations.FirstOrDefault();

                if (location == null)
                    throw new Exception("There are no locations");

                //Create Invoice Object
                var invoice = new AdmCloudInterface.Models.CreditInvoice();
                invoice.DocID = Guid.NewGuid().ToString();
                invoice.DocDate = DateTime.Today;
                invoice.RelationshipID = customer.ID;
                invoice.Notes = "My Notes";
                invoice.CurrencyID = "DOP";
                invoice.ExchangeRate = 1;
                invoice.PaymentTermID = term.ID;
                invoice.LocationID = location.ID;

                invoice.Items.Add(new CreditInvoice_Item { Name = "Test Item", Quantity = 1, Price = 100 });

                //Save Invoice
                var invoice_repository = new AdmCloudInterface.Repositories.CreditInvoicesRepository(token: token);

                var id = await invoice_repository.InsertAsync(invoice);

                _LogInformation($"Internal ID of the registered transaction: {id.ToString()}");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message);
            }
        }

        private async void btnGetToken_Click(object sender, EventArgs e)
        {
            try
            {
                _LogInformation(await _GetToken());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message);
            }
        }
    }
}
