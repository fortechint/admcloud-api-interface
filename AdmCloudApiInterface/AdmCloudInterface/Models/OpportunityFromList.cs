﻿
using System;

namespace AdmCloudInterface.Models
{
    public class OpportunityFromList
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public string Contact { get; set; }

        public string Phone { get; set; }

        public string EMail { get; set; }

        public DateTime DocDate { get; set; }
    }
}
