﻿using System;

namespace AdmCloudInterface.Models
{
    public class SalesOrderFromList
    {
        public Guid ID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
        public Guid RelationshipID { get; set; }
        public string RelationshipName { get; set; }
        public string CurrencyID { get; set; }
        public Guid? LocationID { get; set; }
        public decimal TotalAmount { get; set; }
        public bool Void { get; set; }
        public decimal TotalLocal { get; set; }
    }
}
