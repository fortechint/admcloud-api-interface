﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class CashInvoice
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string Reference { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
        public Guid? CashAccountID { get; set; }
        public Guid? RelationshipID { get; set; }
        public string RelationshipName { get; set; }
        public string FiscalID { get; set; }
        public string NCF { get; set; }
        public string NIF { get; set; }
        public string CurrencyID { get; set; }
        public Guid? RevenueTypeID { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool Void { get; set; }
        public string Notes { get; set; }
        public Guid? EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public Guid? PaymentMethodID { get; set; }
        public string PaymentMethodName { get; set; }
        public int NCF_Sequence { get; set; }
        public string FiscalSequenceTypeID { get; set; }

        public string Beneficiary { get; set; }

        public Guid? LocationID { get; set; }

        public Guid? ShiftID { get; set; }

        public Guid? EmissionPointID { get; set; }

        public List<CashInvoice_Item> Items { get; set; } = new List<CashInvoice_Item>();
        public List<CashInvoice_Applied_Doc> Transactions { get; set; } = new List<CashInvoice_Applied_Doc>();
        public List<CashInvoice_PaymentMethods> PaymentMethods { get; set; } = new List<CashInvoice_PaymentMethods>();

        public List<CashInvoice_CustomField> CustomFields { get; set; } = new List<CashInvoice_CustomField>();
    }

    public class CashInvoice_Item
    {
        public Guid? ItemID { get; set; }
        public string ItemSKU { get; set; }
        public string Name { get; set; }
        public string Comments { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public Guid? UOMID { get; set; }
        public string UOMName { get; set; }
        public decimal DiscountPercent { get; set; }
        public Guid? TaxScheduleID { get; set; }
        public decimal TaxPercent { get; set; }
        public string TaxScheduleName { get; set; }
        public DateTime? SubscriptionDate { get; set; }
        public Guid? AdmCloudCompanyID { get; set; }
    }

    public class CashInvoice_Applied_Doc
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
    }

    public class CashInvoice_PaymentMethods
    {
        public Guid ID { get; set; }
        public int RowOrder { get; set; }
        public Guid PaymentMethodID { get; set; }
        public Guid? AccountID { get; set; }
        public decimal Amount { get; set; }
        public decimal ExchangeRate { get; set; }
        public string Reference { get; set; }
        public string PaymentMethodName { get; set; }
    }

    public class CashInvoice_CustomField
    {
        public string FieldName { get; set; }
        public object Value { get; set; }
    }
}
