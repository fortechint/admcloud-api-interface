﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class Reception
    {
        public Guid ID { get; set; }

        public Guid? SubsidiaryID { get; set; }

        public string Reference { get; set; }

        public string DocID { get; set; }

        public string DocType { get; set; }

        public DateTime DocDate { get; set; }

        public Guid? RelationshipID { get; set; }

        public string RelationshipName { get; set; }

        public bool Void { get; set; }

        public string Notes { get; set; }

        public Guid? LocationID { get; set; }

        public List<Reception_Item> Items { get; set; } = new List<Reception_Item>();
    }

    public class Reception_Item
    {
        public Guid? ItemID { get; set; }
        
        public string ItemSKU { get; set; }
        
        public string Name { get; set; }
        
        public decimal Quantity { get; set; }
        
        public decimal Cost { get; set; }
        
        public Guid? UOMID { get; set; }
        
        public string UOMName { get; set; }
    }
}
