﻿
using System;

namespace AdmCloudInterface.Models
{
    public class JournalFromList
    {
        public Guid ID { get; set; }

        public string DocID { get; set; }

        public DateTime DocDate { get; set; }

        public string CurrencyID { get; set; }

        public bool Void { get; set; }
    }
}
