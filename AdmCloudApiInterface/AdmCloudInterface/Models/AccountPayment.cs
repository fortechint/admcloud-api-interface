﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class AccountPayment
    {
        public Guid ID { get; set; }

        public Guid? SubsidiaryID { get; set; }

        public string DocID { get; set; }

        public DateTime DocDate { get; set; }

        public decimal ExchangeRate { get; set; }

        public string CurrencyID { get; set; }

        public string Beneficiary { get; set; }

        public Guid? RelationshipID { get; set; }

        public Guid? PaymentTypeID { get; set; }

        public Guid? CashAccountID { get; set; }

        public string Notes { get; set; }

        public List<AccountPayment_Item> Items { get; set; } = new List<AccountPayment_Item>();
    }

    public class AccountPayment_Item
    {
        public Guid AccountID { get; set; }

        public Guid? RelationshipID { get; set; }

        public decimal ExchangeRate { get; set; }

        public decimal Price { get; set; }

        public decimal Quantity { get; set; }
    }

}
