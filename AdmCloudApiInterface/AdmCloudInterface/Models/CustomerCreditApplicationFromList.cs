﻿using System;

namespace AdmCloudInterface.Models
{
    public class CustomerCreditApplicationFromList
    {
        public Guid ID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
        public Guid RelationshipID { get; set; }
        public bool Void { get; set; }
    }
}
