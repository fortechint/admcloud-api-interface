﻿using System;

namespace AdmCloudInterface.Models
{
    public class ItemFromList
    {
        public Guid ID { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
      
    }
}
