﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class Project_File
    {
        public string Uri { get; set; }
        public string Name { get; set; }
        public string CustomerName { get; set; }
    }

    public class Project
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RelationshipID { get; set; }
        public string Manager { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PO { get; set; }
        public Guid? JobClassID { get; set; }
        public int Status { get; set; }
        public string CurrencyID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAndName { get; set; }

        public List<Project_File> Files { get; set; } = new List<Project_File>();
    }
}
