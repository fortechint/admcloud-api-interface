﻿using System;

namespace AdmCloudInterface.Models
{
    public class Currency
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public decimal ExchangeRate { get; set; }
        public string Notes { get; set; }
        public string Sign { get; set; }
        public string Fraction { get; set; }
        public Guid? ARAccountID { get; set; }
        public Guid? APAccountID { get; set; }
        public Guid? EmployeeAPAccountID { get; set; }
        public bool IsFunctionalCurrency { get; set; }
    }
}
