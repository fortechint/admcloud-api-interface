﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class SalesOrder
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
        public Guid? RelationshipID { get; set; }
        public string RelationshipName { get; set; }
        public string FiscalID { get; set; }
        public string CurrencyID { get; set; }
        public decimal TotalAmount { get; set; }
        public bool Void { get; set; }
        public string Notes { get; set; }
        public Guid? EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public Guid? PaymentTermID { get; set; }
        public string Reference { get; set; }
        public Guid? LocationID { get; set; }

        public string PaymentTermName { get; set; }

        public List<SalesOrder_CustomFields> CustomFields { get; set; } = new List<SalesOrder_CustomFields>();
        public List<SalesOrder_Item> Items { get; set; } = new List<SalesOrder_Item>();
        public List<SalesOrder_Applied_Doc> Transactions { get; set; } = new List<SalesOrder_Applied_Doc>();
    }

    public class SalesOrder_CustomFields
    {
        public object Value { get; set; }
        public string FieldName { get; set; }

        //No requerido para ACTUALIZACION
        public string Caption { get; set; }
    }

    public class SalesOrder_Item
    {
        public Guid? ItemID { get; set; }
        public string ItemSKU { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public Guid? UOMID { get; set; }
        public string UOMName { get; set; }
        public decimal DiscountPercent { get; set; }
        public Guid? TaxScheduleID { get; set; }
        public decimal TaxPercent { get; set; }
        public string TaxScheduleName { get; set; }
    }

    public class SalesOrder_Applied_Doc
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
    }
}
