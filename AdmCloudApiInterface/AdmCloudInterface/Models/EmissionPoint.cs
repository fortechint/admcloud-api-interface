﻿using System;

namespace AdmCloudInterface.Models
{
    public class EmissionPoint
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }
}
