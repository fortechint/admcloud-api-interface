﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class Item
    {
        public Guid ID { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
        public string SalesDescription { get; set; }
        public string PurchaseDescription { get; set; }
        public string ItemType { get; set; }
        public string BarCode { get; set; }
        public Guid? ItemClassID { get; set; }
        public Guid? UOMPlanID { get; set; }
        public Guid? PurchaseUOMID { get; set; }
        public Guid? SalesUOMID { get; set; }
        public Guid? ReportUOMID { get; set; }
        public string ReportUOMName { get; set; }
        public bool Inactive { get; set; }
        public string Notes { get; set; }
        public bool RequiresLots { get; set; }
        public bool Serializable { get; set; }
        public bool RequiresBins { get; set; }
        public bool Assembly { get; set; }
        public decimal Weight { get; set; }
        public string WeightUOM { get; set; }
        public Guid? TaxScheduleID { get; set; }
        public Guid? OriginID { get; set; }
        public bool ProductionItem { get; set; }
        public bool RawMaterial { get; set; }
        public bool SalesItem { get; set; }
        public bool PurchaseItem { get; set; }
        public bool SparePart { get; set; }
        public Guid? WebStore { get; set; }
        public string WebStoreDescription { get; set; }
        public string WebStoreDetailedDescription { get; set; }
        public Guid? ItemFamilyID { get; set; }
        public Guid? WebStoreImage { get; set; }
        public Guid? BrandID { get; set; }
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public string DimensionsUOM { get; set; }
        public decimal PurchasePrice { get; set; }
        public string PurchasePriceCurrencyID { get; set; }
        public decimal Cost { get; set; }
        public Guid? Sales_AccountID { get; set; }
        public Guid? Inventory_AccountID { get; set; }
        public Guid? Cost_Sales_AccountID { get; set; }
        public Guid? Sales_Discount_AccountID { get; set; }
        public Guid? Sales_Returns_AccountID { get; set; }
        public Guid? Adjustments_AccountID { get; set; }
        public Guid? Attribute1ID { get; set; }
        public Guid? Attribute2ID { get; set; }
    }
}
