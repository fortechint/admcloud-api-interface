﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class Account
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public int AccountClassID { get; set; }
        public bool IsCashAccount { get; set; }
        public string AccountClassName { get; set; }
        public string AccountType { get; set; }
        public string AccountTypeName { get; set; }
        public int AccountTypeID { get; set; }
        public string GroupID { get; set; }
        public string Notes { get; set; }
        public bool Inactive { get; set; }
        public string ExpenseTypeID { get; set; }
        public string CurrencyID { get; set; }
        public bool GroupAccount { get; set; }
        public string ParentAccountID { get; set; }
        public string SubsidiaryID { get; set; }
        public int GoodOrService { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BankFiscalID { get; set; }
        public bool AutoNumberChecks { get; set; }
        public int NextCheckNumber { get; set; }
        public int BankAccountType { get; set; }
        public string CheckFormatID { get; set; }
        public string SubsidiaryName { get; set; }
        public decimal ExchangeRate { get; set; }
    }
}
