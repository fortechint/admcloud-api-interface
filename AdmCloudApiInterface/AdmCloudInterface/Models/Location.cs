﻿using System;

namespace AdmCloudInterface.Models
{
    public class Location
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string SubsidiaryName { get; set; }
    }
}
