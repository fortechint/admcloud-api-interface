﻿using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class Journal
    {
        public Guid ID { get; set; }

        public Guid? SubsidiaryID { get; set; }

        public string DocID { get; set; }

        public DateTime DocDate { get; set; }

        public string CurrencyID { get; set; }

        public decimal ExchangeRate { get; set; }

        public bool Void { get; set; }

        public string Notes { get; set; }

        public List<Journal_Account> Accounts { get; set; } = new List<Journal_Account>();
    }

    public class Journal_Account
    {
        public Guid AccountID { get; set; }

        public Guid? DepartmentID { get; set; }

        public Guid? DivisionID { get; set; }

        public Guid? ItemClassID { get; set; }

        public Guid? LocationID { get; set; }

        public decimal Debit { get; set; }

        public decimal Credit { get; set; }
    }

}
