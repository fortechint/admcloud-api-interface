﻿using System;

namespace AdmCloudInterface.Models
{
    public class CustomerFromList
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsCustomer { get; set; }
        public bool IsEmployee { get; set; }
        public bool IsVendor { get; set; }
        public bool IsPartner { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string EMail { get; set; }
        public string Notes { get; set; }
        public bool Inactive { get; set; }
        public string FiscalID { get; set; }
        public int FiscalIDType { get; set; }
        public decimal DiscountPercent { get; set; }
        public string WebPage { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string LinkedIn { get; set; }
        public Guid? PaymentTermID { get; set; }
        public int EmployeeQuantity { get; set; }
        public decimal PotentialVolume { get; set; }
        public string Profile { get; set; }
        public string CurrencyID { get; set; }
        public string TaxScheduleID { get; set; }
        public string TaxRetentionGroupID { get; set; }
        public string ComercialName { get; set; }
        public string BusinessActivity { get; set; }
        public string ParentRelationshipID { get; set; }
        public decimal CreditLimit { get; set; }
        public string SalesTerritoryID { get; set; }
        public int Conditions { get; set; }
        public string CustomerClassID { get; set; }
        public string SalesRepID { get; set; }
        public string PriceLevelID { get; set; }
        public bool Prospect { get; set; }
        public string StatusID { get; set; }
        public string ItemClassID { get; set; }
        public int Sales_NCF_Sequence { get; set; }
        public string SalesFiscalSequenceTypeID { get; set; }
        public int PaymentMethodID { get; set; }
        public string BankAccountID { get; set; }
        public int BankAccountType { get; set; }
        public string BankAccountNumber { get; set; }
        public string VendorClassID { get; set; }
        public int Purchase_NCF_Sequence { get; set; }
        public string PurchasesFiscalSequenceTypeID { get; set; }
        public bool IsSalesRep { get; set; }
        public bool IsTechnitian { get; set; }
        public bool IsMechanic { get; set; }
        public bool IsManufacturingOperator { get; set; }
        public decimal HourCost { get; set; }
        public string EmissionPointID { get; set; }
        public string EmployeeClassID { get; set; }
        public bool AllowAccess { get; set; }
        public string AccessControlCardID { get; set; }
        public string ARAccountID { get; set; }
        public string APAccountID { get; set; }
        public string PurchaseTransitAccountID { get; set; }
        public string ReimbursementsAccountID { get; set; }
        public string Image { get; set; }
       
        public string FullName { get; set; }

        public string ImageName { get; set; }

        public string ImageUri { get; set; }
    }
}
