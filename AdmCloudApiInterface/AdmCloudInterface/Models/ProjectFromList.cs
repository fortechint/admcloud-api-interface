﻿using System;

namespace AdmCloudInterface.Models
{
    public class ProjectFromList
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string RelationshipID { get; set; }
        public string Manager { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PO { get; set; }
        public Guid? JobClassID { get; set; }
        public int Status { get; set; }
        public string CurrencyID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAndName { get; set; }
    }
}
