﻿using System;

namespace AdmCloudInterface.Models
{
    public class PaymentTerm
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }
}
