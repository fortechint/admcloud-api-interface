﻿
using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class CustomerCreditApplication
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
        public bool Void { get; set; }

        public List<CustomerCreditApplication_Doc> Documents { get; set; } = new List<CustomerCreditApplication_Doc>();
    }

    public class CustomerCreditApplication_Doc
    {
        public Guid DocumentID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
    }
}
