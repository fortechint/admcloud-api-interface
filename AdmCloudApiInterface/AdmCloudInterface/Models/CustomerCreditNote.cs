﻿
using System;
using System.Collections.Generic;

namespace AdmCloudInterface.Models
{
    public class CustomerCreditNote
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string DocID { get; set; }
        public string Reference { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
        public Guid? RelationshipID { get; set; }
        public string RelationshipName { get; set; }
        public Guid? LocationID { get; set; }
        public string FiscalID { get; set; }
        public string NCF { get; set; }
        public string NIF { get; set; }
        public Guid? InvoiceID { get; set; }
        public string CurrencyID { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool Void { get; set; }
        public decimal AppliedPayments { get; set; }
        public string Notes { get; set; }
        public Guid? EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string FiscalSequenceTypeID { get; set; }

        public Guid? EmissionPointID { get; set; }

        public List<CustomerCreditNote_Item> Items { get; set; } = new List<CustomerCreditNote_Item>();
        public List<CustomerCreditNote_Applied_Doc> Transactions { get; set; } = new List<CustomerCreditNote_Applied_Doc>();
    }

    public class CustomerCreditNote_Item
    {
        public Guid? ItemID { get; set; }
        public string ItemSKU { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public Guid? UOMID { get; set; }
        public string UOMName { get; set; }
        public decimal DiscountPercent { get; set; }
        public Guid? TaxScheduleID { get; set; }
        public decimal TaxPercent { get; set; }
        public string TaxScheduleName { get; set; }
    }

    public class CustomerCreditNote_Applied_Doc
    {
        public Guid ID { get; set; }
        public Guid? SubsidiaryID { get; set; }
        public string DocID { get; set; }
        public string DocType { get; set; }
        public DateTime DocDate { get; set; }
    }
}
