﻿using System;

namespace AdmCloudInterface.Models
{
    public class PaymentMethod
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }
}
