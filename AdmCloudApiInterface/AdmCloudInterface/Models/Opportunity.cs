﻿using System;

namespace AdmCloudInterface.Models
{
    public class Opportunity
    {
        public Guid ID { get; set; }

        public Guid? SubsidiaryID { get; set; }

        public string Name { get; set; }

        public string Contact { get; set; }

        public string Phone { get; set; }

        public string EMail { get; set; }

        public string CurrencyID { get; set; }

        public DateTime DocDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public Guid? SalesStageID { get; set; }

        public Guid? ItemClassID { get; set; }

        public string Notes { get; set; }

        public DateTime? EstimatedCloseDate { get; set; }
    }
}
