﻿using System;

namespace AdmCloudInterface.Models
{
    public class Subsidiary
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }
}
