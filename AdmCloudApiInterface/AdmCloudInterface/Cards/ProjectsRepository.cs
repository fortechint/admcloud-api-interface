﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AdmCloudInterface.Models;
using System.Text;

namespace AdmCloudInterface.Repositories
{
    public class ProjectsRepository : BaseRepository
    {
        private const string BASE_URL = "Projects";

        public ProjectsRepository(string token, string apiBase = Config.Constants.API_BASE)
        {
            _token = token;
            _apiBase = apiBase;
        }

        public ProjectsRepository(string mobileAppToken, string email, string password,
            string company, string role, string apiBase = Config.Constants.API_BASE)
        {
            _mobileAppToken = mobileAppToken;
            _email = email;
            _password = password;
            _company = company;
            _role = role;
            _apiBase = apiBase;
        }

        public async Task<List<ProjectFromList>> GetListAsync(int skip = 0)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Projects?skip={skip}&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var response = await client.GetAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.ProjectFromList>>(invoicesString);

            //Retornar
            return list;
        }

        public async Task<List<ProjectFromList>> GetListForManagerAsync(string ManagerEMail)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Projects/ListForManager?ManagerMail={ManagerEMail}&skip=0&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var response = await client.GetAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.ProjectFromList>>(invoicesString);

            //Retornar
            return list;
        }

        public async Task<Project> GetAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Projects?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.GetAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            if (returnedMsg.data == null)
                throw new Exception("Documento no existe");

            string invoicesString = returnedMsg.data.ToString();
            var doc = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Project>(invoicesString);

            //Retornar
            return doc;
        }

        public async Task<Guid> LinkFileAsync(Guid ProjectID, Guid FileID, string Name)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Projects/LinkFile?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ProjectID}&fileid={FileID}&name={Name}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject("");
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string guidString = returnedMsg.data.ToString();
            Guid ID = Guid.Parse(guidString);

            //Retornar
            return ID;
        }

        public async Task<Guid> InsertAsync(Subsidiary record)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}{BASE_URL}?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(record);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string guidString = returnedMsg.data.ToString();
            Guid ID = Guid.Parse(guidString);

            //Retornar
            return ID;
        }

        public async Task DeleteAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}{BASE_URL}/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.DeleteAsync(url);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }
    }
}
