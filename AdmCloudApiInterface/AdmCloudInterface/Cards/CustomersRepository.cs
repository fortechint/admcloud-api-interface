﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AdmCloudInterface.Models;
using System.Text;

namespace AdmCloudInterface.Repositories
{
    public class CustomersRepository : BaseRepository
    {
        public CustomersRepository(string token, string apiBase = Config.Constants.API_BASE)
        {
            _token = token;
            _apiBase = apiBase;
        }

        public CustomersRepository(string mobileAppToken, string email, string password,
            string company, string role, string apiBase = Config.Constants.API_BASE)
        {
            _mobileAppToken = mobileAppToken;
            _email = email;
            _password = password;
            _company = company;
            _role = role;
            _apiBase = apiBase;
        }

        public async Task<List<CustomerFromList>> GetListAsync(int skip = 0)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Customers?skip={skip}&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.CustomerFromList>>(invoicesString);

            //Retornar
            return list;
        }

        public async Task<Customer> GetAsync(Guid? ID = null, string name = null, string email = null)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Customers?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            if (ID != null) url += $"&id={ID}";
            if (name != null) url += $"&name={name}";
            if (email != null) url += $"&email={email}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            if (returnedMsg.data == null)
                throw new Exception("Documento no existe");

            string invoicesString = returnedMsg.data.ToString();
            var doc = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Customer>(invoicesString);

            //Retornar
            return doc;
        }

        public async Task<Guid> InsertAsync(Customer record)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Customers?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(record);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string guidString = returnedMsg.data.ToString();
            Guid ID = Guid.Parse(guidString);

            //Retornar
            return ID;
        }

        public async Task DeleteAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Customers/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.DeleteAsync(url);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }
    }
}
