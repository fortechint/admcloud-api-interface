﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AdmCloudInterface.Models;

namespace AdmCloudInterface.Repositories
{
    public class CustomerCreditApplicationsRepository: BaseRepository
    {
        private const string BASE_URL = "CustomerCreditApplications";

        public CustomerCreditApplicationsRepository(string token, string apiBase = Config.Constants.API_BASE)
        {
            _token = token;
            _apiBase = apiBase;
        }

        public CustomerCreditApplicationsRepository(string mobileAppToken, string email, string password,
            string company, string role, string apiBase = Config.Constants.API_BASE)
        {
            _mobileAppToken = mobileAppToken;
            _email = email;
            _password = password;
            _company = company;
            _role = role;
            _apiBase = apiBase;
        }

        public async Task<List<CustomerCreditApplicationFromList>> GetListAsync(int skip = 0)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CustomerCreditApplications?skip={skip}&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var response = await client.GetAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.CustomerCreditApplicationFromList>>(invoicesString);

            //Retornar
            return list;
        }

        public async Task<CustomerCreditApplication> GetAsync(Guid? ID = null, string DocID = null)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}{BASE_URL}?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            if (ID != null) url += $"&ID={ID}";
            if (DocID != null) url += $"&DocID={DocID}";

            var response = await client.GetAsync(url);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            if (returnedMsg.data == null)
                throw new Exception("Documento no existe");

            string invoicesString = returnedMsg.data.ToString();
            var doc = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.CustomerCreditApplication>(invoicesString);

            //Retornar
            return doc;
        }

        public async Task DeleteAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CustomerCreditApplications/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.DeleteAsync(url);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }
    }
}
