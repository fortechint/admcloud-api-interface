﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AdmCloudInterface.Models;
using System.Text;
using System.Net.Http.Headers;

namespace AdmCloudInterface.Repositories
{
    public class CreditInvoicesRepository: BaseRepository
    {
        private const string BASE_URL = "CreditInvoices";

        public CreditInvoicesRepository(string token, string apiBase = Config.Constants.API_BASE)
        {
            _token = token;
            _apiBase = apiBase;
        }

        public CreditInvoicesRepository(string mobileAppToken, string email, string password,
            string company, string role, string apiBase = Config.Constants.API_BASE)
        {
            _mobileAppToken = mobileAppToken;
            _email = email;
            _password = password;
            _company = company;
            _role = role;
            _apiBase = apiBase;
        }

        public async Task<List<CreditInvoiceFromList>> GetListAsync(int skip = 0,
            Guid? LocationID = null, DateTime? DateFrom = null, DateTime? DateTo = null,
            string search = "", string DocID = "", string NCF = "")
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices?skip={skip}&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&LocationID={LocationID}&DateFrom={DateFrom?.ToString("yyyy-MM-dd")}&DateTo={DateTo?.ToString("yyyy-MM-dd")}&search={search}&DocID={DocID}&NCF={NCF}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.CreditInvoiceFromList>>(invoicesString);

            //Retornar
            return list;
        }

        public async Task<List<CreditInvoiceFromList>> GetUnPrintedListAsync(Guid? LocationID = null, 
            DateTime? DateFrom = null, DateTime? DateTo = null, int skip = 0,
            string search = "", string DocID = "", string NCF = "")
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices/Unprinted?skip={skip}&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&LocationID={LocationID}&DateFrom={DateFrom?.ToString("yyyy-MM-dd")}&DateTo={DateTo?.ToString("yyyy-MM-dd")}&search={search}&DocID={DocID}&NCF={NCF}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.CreditInvoiceFromList>>(invoicesString);


            //Retornar
            return list;
        }

        public async Task<List<CreditInvoiceFromList>> GetPrintedListAsync(Guid? LocationID = null,
            DateTime? DateFrom = null, DateTime? DateTo = null, int skip = 0,
            string search = "", string DocID = "", string NCF = "")
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices/Printed?skip={skip}&token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&LocationID={LocationID}&DateFrom={DateFrom?.ToString("yyyy-MM-dd")}&DateTo={DateTo?.ToString("yyyy-MM-dd")}&search={search}&DocID={DocID}&NCF={NCF}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string invoicesString = returnedMsg.data.ToString();
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.CreditInvoiceFromList>>(invoicesString);

            //Retornar
            return list;
        }

        public async Task<CreditInvoice> GetAsync(Guid? ID = null, string DocID = null)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}{BASE_URL}?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            if (ID != null) url += $"&ID={ID}";
            if (DocID != null) url += $"&DocID={DocID}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            if (returnedMsg.data == null)
                throw new Exception("Documento no existe");

            string invoicesString = returnedMsg.data.ToString();
            var doc = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.CreditInvoice>(invoicesString);

            //Retornar
            return doc;
        }

        public async Task<Guid> InsertAsync(CreditInvoice record)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(record);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string guidString = returnedMsg.data.ToString();
            Guid ID = Guid.Parse(guidString);

            //Retornar
            return ID;
        }

        public async Task UpdateAsync(CreditInvoice record)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(record);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PutAsync(url, content);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }

        public async Task DeleteAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.DeleteAsync(url);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }

        public async Task VoidAsync(Guid ID, Guid? cancellation_type)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices/Void/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}&cancellation_type={cancellation_type}";

            var response = await client.PostAsync(url, null);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }

        public async Task MarkPrintedAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}CreditInvoices/MarkPrinted/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.PostAsync(url, null);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }
    }
}
