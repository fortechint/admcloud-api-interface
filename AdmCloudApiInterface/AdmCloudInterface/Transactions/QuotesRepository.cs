﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AdmCloudInterface.Models;
using System.Text;
using System.Net.Http.Headers;

namespace AdmCloudInterface.Repositories
{
    public class QuotesRepository : BaseRepository
    {
        private const string BASE_URL = "Quotes";

        public QuotesRepository(string token, string apiBase = Config.Constants.API_BASE)
        {
            _token = token;
            _apiBase = apiBase;
        }

        public QuotesRepository(string mobileAppToken, string email, string password,
            string company, string role, string apiBase = Config.Constants.API_BASE)
        {
            _mobileAppToken = mobileAppToken;
            _email = email;
            _password = password;
            _company = company;
            _role = role;
            _apiBase = apiBase;
        }

        public async Task<Quote> GetAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}{BASE_URL}?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            if (ID != null) url += $"&ID={ID}";

            var response = await client.GetAsync(url);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            if (returnedMsg.data == null)
                throw new Exception("Documento no existe");

            string invoicesString = returnedMsg.data.ToString();

            var doc = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Quote>(invoicesString);

            //Retornar
            return doc;
        }

        public async Task<Guid> InsertAsync(Quote record)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Quotes?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(record);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string guidString = returnedMsg.data.ToString();
            Guid ID = Guid.Parse(guidString);

            //Retornar
            return ID;
        }

        public async Task<Guid> UpdateAsync(Quote record)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Quotes?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(record);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PutAsync(url, content);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            string guidString = returnedMsg.data.ToString();
            Guid ID = Guid.Parse(guidString);

            //Retornar
            return ID;
        }

        public async Task DeleteAsync(Guid ID)
        {
            //Llamar Web Service
            var client = new HttpClient();

            if (!string.IsNullOrEmpty(_email) || !string.IsNullOrEmpty(_password))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));
            }

            string url = $"{_apiBase}Quotes/?token={_token}&MobileAppToken={_mobileAppToken}&company={_company}&role={_role}&id={ID}";

            var response = await client.DeleteAsync(url);
            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }
    }
}
