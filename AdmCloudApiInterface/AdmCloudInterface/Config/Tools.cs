﻿
namespace AdmCloudInterface.Config
{
    public class Tools
    {
        public enum NCF_Sequence
        {
            NoGenerar = 0,
            ConsumidorFinal = 1,
            CreditoFiscal = 2,
            Gubernamental = 3,
            Especial = 4
        }
    }
}
