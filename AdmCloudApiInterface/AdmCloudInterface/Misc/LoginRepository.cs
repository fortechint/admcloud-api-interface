﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AdmCloudInterface.Models;
using System.Net.Http.Headers;
using System.Text;

namespace AdmCloudInterface.Repositories
{
    public class LoginRepository : BaseRepository
    {
        private Guid _roleid;
        private Guid _appid;

        public LoginRepository(string email, string password, string company, string role, Guid roleid, Guid appid,
            string apiBase = Config.Constants.API_BASE)
        {
            _email = email;
            _password = password;
            _company = company;
            _appid = appid;
            _role = role;
            _roleid = roleid;
            _apiBase = apiBase;
        }

        public async Task LoginAsync()
        {
            //Llamar Web Service
            var client = new HttpClient();

            string url = $"{_apiBase}Login?company={_company}&appid={_appid}";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject("");
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var errorObject = new ApiResponse();
                errorObject.success = false;
                errorObject.message = "Usuario o clave inválidos";
                throw new ApplicationException(errorObject.message);
            }

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var errorObject = new ApiResponse();
                errorObject.message = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString).message;
                throw new ApplicationException(errorObject.message);
            }

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);
        }

        public async Task<string> GetTokenAsync()
        {
            var client = new HttpClient();

            string url = $"{_apiBase}Token/?company={_company}&appid={_appid}&roleid={_roleid}&role={_role}";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetAuthHeader(_email, _password));

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject("");
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(url, content);
            await base.ProcessBadResponse(response);

            var apiResponseString = await response.Content.ReadAsStringAsync();

            var returnedMsg = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiResponseString);

            if (!returnedMsg.success)
                throw new Exception(returnedMsg.message);

            return returnedMsg.data?.ToString();
        }
    }
}
