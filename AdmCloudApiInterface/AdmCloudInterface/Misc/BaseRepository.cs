﻿using AdmCloudInterface.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AdmCloudInterface.Repositories
{
    public class BaseRepository
    {
        protected string _token;
        protected string _apiBase = Config.Constants.API_BASE;
        protected string _mobileAppToken;
        protected string _email;
        protected string _password;
        protected string _company;
        protected string _role;

        protected string GetAuthHeader(string email, string password)
        {
            var header = Convert.ToBase64String(
            System.Text.ASCIIEncoding.ASCII.GetBytes(
                string.Format("{0}:{1}", email, password)));

            return header;
        }

        protected async Task ProcessBadResponse(HttpResponseMessage response)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new ApplicationException("No autorizado");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                var apiBadResponseString = await response.Content.ReadAsStringAsync();
                var return_object = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(apiBadResponseString);

                var message = return_object.message;

                var errorObject = new ApiResponse();
                errorObject.message = message;
                throw new ApplicationException(errorObject.message);
            }
            else if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var errorObject = new ApiResponse();
                errorObject.message = "Error: " + response.StatusCode.ToString();
                throw new ApplicationException(errorObject.message);
            }
        }
    }
}
